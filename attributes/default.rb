# NTP Defaults
default['ntp']['servers'] = ["time.apple.com"]

# Client Options (Set client to false if cooking a DC)
default['samba4-ad']['client'] = false
default['samba4-ad']['client_join_domain'] = false

# Data Bag for Credentials
default['samba4-ad'].tap do |node|
  node['chef-vault']    = false
  node['data-bag']      = 'samba4-creds'
  node['data-bag-item'] = 'reedfamilyninjas.com'
end

default['samba4-ad']['nameservers'] = ['127.0.0.1'] # Used in /etc/resolv.conf config
                                                    # Be sure to add the other DC (if available).
# DC Configuration
default['samba4-ad'].tap do |node|
  node['dc-role']       = 'standalone' # standalone -or- secondary

  node['hostname']      = 'dc1'
  node['join-server']   = 'dc1.reedfamilyninjas.com'
  node['resolvconf_iface_order']  = [ 'lo' ]
                                        
  # Functional Level
  node['functional-level'] = '2008_R2'  # 2003 | 2008 | 2008_R2 (Options with Ubuntu 14.04)

  # AD Sites
  node['site']          = 'DefaultSite'

  # DNS / Domain / Kerberos Config
  node['domain-fqdn']   = 'reedfamilyninjas.com'
  node['domain']        = 'REEDFAMILY'
  node['realm']         = 'REEDFAMILYNINJAS.COM'
  node['dns-backend']   = 'BIND9_DLZ'
  node['samba-role']    = 'dc'
end

# Bind Options
default['samba4-ad'].tap do |node|
  node['forwarders'] = [ '8.8.8.8', '4.2.2.1' ]
end

# Packages
default['samba4-ad'].tap do |node|
  node['install_bind']    = true
  node['configure_ntp']   = true

  node['client_packages'] = %w(sssd krb5-user)
  node['smb_packages']    = %w(samba smbclient winbind)
  node['bind_packages']   = %w(bind9 bind9utils bind9-host)
  node['packages']        = %w(attr build-essential libacl1-dev libattr1-dev libblkid-dev libgnutls-dev libreadline-dev python-dev libpam0g-dev python-dnspython gdb pkg-config libpopt-dev libldap2-dev dnsutils libbsd-dev attr krb5-user docbook-xsl libcups2-dev acl ldb-tools)
end

default['samba4-ad'].tap do |node|
  node['timezone'] = 'EST'
end
