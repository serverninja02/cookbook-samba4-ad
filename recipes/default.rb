#
# Cookbook Name:: samba4-ad
# Recipe:: default
#
# Copyright (C) 2016 SpinDance, Inc.
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt'

#Set Timezone
execute "Set Timezone" do
  command "timedatectl set-timezone #{node['samba4-ad']['timezone']}"
end

#Install / Configure NPT
include_recipe 'ntp' if node['samba4-ad']['configure_ntp']

#Update NTP
service "ntp" do
  action :stop
end

execute "ntpdate -s #{node['ntp']['servers'][0]}"

service "ntp" do
  action :start
end

#Pre-requisites
node['samba4-ad']['packages'].each do |pkg|
  package pkg
end

#Install required samba packages
node['samba4-ad']['smb_packages'].each do |pkg|
  package pkg
end

if node['samba4-ad']['client']
  include_recipe 'samba4-ad::client'
else
  #Install / configure DC
  
  #Install / Configure Bind9
  include_recipe 'samba4-ad::bind' if node['samba4-ad']['install_bind']
  
  #Configure DC
  if node['samba4-ad']['dc-role'] === 'standalone'
    include_recipe 'samba4-ad::dc_standalone'
  else
    include_recipe 'samba4-ad::dc_secondary'
  end

  #Fix permissions for bind
  file '/var/lib/samba/private/dns.keytab' do
    user 'root'
    group 'bind'
    mode '0640'
    only_if { node['samba4-ad']['install_bind'] }
  end
  
  directory '/var/lib/samba/private/dns' do
    user 'bind'
    group 'bind'
    mode '0775'
    only_if { node['samba4-ad']['install_bind'] }
  end

end

#Reload apparmor
service "apparmor" do
  action :reload
end
