# Chef vault
if node['samba4-ad']['chef-vault']
  include_recipe 'chef-vault'
  secrets = chef_vault_item(node['samba4-ad']['data-bag'], node['samba4-ad']['data-bag-item'])
else
  secrets = data_bag_item(node['samba4-ad']['data-bag'], node['samba4-ad']['data-bag-item'])
end

# Remove smb.conf
file '/etc/samba/smb.conf' do
  action :delete
  not_if ("grep \"server role = active directory\" /etc/samba/smb.conf")
end

# Temporary Kerberos Config
template '/etc/krb5.conf' do
  not_if ("grep \"server role = active directory\" /etc/samba/smb.conf")
end

# Provision Active Directory
execute "samba-tool domain join" do
  iface = node['network']['default_interface']
  command <<-EOH
/usr/bin/samba-tool domain join #{node['samba4-ad']['domain-fqdn']} DC \
-U#{secrets['adminuser']} \
--realm=#{node['samba4-ad']['realm']} \
--adminpass='#{secrets['adminpass']}' \
--password='#{secrets['adminpass']}' \
--server='#{node['samba4-ad']['join-server']}' \
--option="interfaces=lo #{iface}" \
--site=#{node['samba4-ad']['site']} \
--dns-backend=#{node['samba4-ad']['dns-backend']}
  EOH
  live_stream true
  not_if ("grep \"server role = active directory\" /etc/samba/smb.conf")
end

# Final Kerberos Config
file "/etc/krb5.conf" do
  action :delete
  not_if { File::symlink?("/etc/krb5.conf") }
end

link "/etc/krb5.conf" do
  to "/var/lib/samba/private/krb5.conf"
end
