#Chef vault
if node['samba4-ad']['chef-vault']
  include_recipe 'chef-vault'
  secrets = chef_vault_item(node['samba4-ad']['data-bag'], node['samba4-ad']['data-bag-item'])
else
  secrets = data_bag_item(node['samba4-ad']['data-bag'], node['samba4-ad']['data-bag-item'])
end

#Remove smb.conf
file '/etc/samba/smb.conf' do
  action :delete
  not_if ("grep \"server role = active directory\" /etc/samba/smb.conf")
end

#Provision Active Directory
execute "samba-tool domain provision" do
  iface = node['network']['default_interface']
  command <<-EOH
/usr/bin/samba-tool domain provision \
--use-rfc2307 \
--domain=#{node['samba4-ad']['domain']} \
--realm=#{node['samba4-ad']['realm']} \
--adminpass='#{secrets['adminpass']}' \
--ldapadminpass='#{secrets['adminpass']}' \
--krbtgtpass='#{secrets['adminpass']}' \
--function-level='#{node['samba4-ad']['functional-level']}' \
--server-role=#{node['samba4-ad']['samba-role']} \
--option="interfaces=127.0.0.0/8 #{iface}" \
--option="kerberos method = system keytab" \
--option="client ldap sasl wrapping = sign" \
--option="allow dns updates = nonsecure and secure" \
--option="nsupdate command =  /usr/bin/nsupdate -g" \
--site=#{node['samba4-ad']['site']} \
--dns-backend=#{node['samba4-ad']['dns-backend']}
  EOH
  live_stream true
  not_if ("grep \"server role = active directory\" /etc/samba/smb.conf")
end

#Kerberos Config
file "/etc/krb5.conf" do
  action :delete
  not_if { File::symlink?("/etc/krb5.conf") }
end

link "/etc/krb5.conf" do
  to "/var/lib/samba/private/krb5.conf"
end
