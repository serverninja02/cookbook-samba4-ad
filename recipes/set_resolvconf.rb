include_recipe 'resolvconf::install'

resolvconf 'custom' do
  nameserver node['samba4-ad']['nameservers']
  search     node['samba4-ad']['domain-fqdn']
  head       "# Don't touch this configuration file!"
  base       "# Will be added after nameserver, search, options config items"
  tail       "# This goes to the end of the file."
  if node['samba4-ad']['client']
    interface_order ["#{node['network']['default_interface']}"] 
  else
    interface_order ['lo'] 
  end
  clear_dns_from_interfaces true unless node['samba4-ad']['client']
end
