iface = node['network']['default_interface']
gateway = node['network']['default_gateway']
netmask = ""
ip = ""

log "iface: #{iface}"
log "gw: #{gateway}"

interface = node['network']['interfaces'][iface]

interface['addresses'].each do |ipaddr, params|
    ip = ipaddr if params['family'].eql?('inet')
    netmask = params['netmask'] if params['family'].eql?('inet')
end

log "ip: #{ip}"
log "netmask: #{netmask}"

ifconfig ip do
  mask netmask
  device iface
end

route '0.0.0.0/0' do
  gateway gateway
  device iface
end

#template "/etc/network/interfaces.d/ifcfg-#{iface}" do
#  source "ifcfg-eth.erb"
#  owner 'root'
#  group 'root'
#  mode '0644'
#  variables ({
#    "addr" => ip,
#    "mask" => netmask,
#    "iface" => iface,
#    "dns" => node['samba4-ad']['nameservers']
#  })
#end
