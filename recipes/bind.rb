# Install Packaqges
node['samba4-ad']['bind_packages'].each do |pkg|
  package pkg
end

template '/etc/apparmor.d/usr.sbin.named' do
  user 'root'
  group 'root'
  notifies :reload, 'service[apparmor]', :delayed
end

template '/etc/bind/named.conf' do
  user 'root'
  group 'root'
end

template '/etc/bind/named.conf.options' do
  user 'root'
  group 'root'
end
