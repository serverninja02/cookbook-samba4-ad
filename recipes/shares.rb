# Samba smb.conf
template '/etc/samba/smb.conf' do
  source 'smb.conf.erb'
  owner 'root'
  group 'root'
  mode 00644
  variables :shares => node['samba4-ad']['shares']
  ['smbd', 'nmbd'].each do |s|
    notifies :restart, "service[#{s}]"
  end
end

# Ensure directories were created
node['samba4-ad']['shares'].each do |sharename, params|
  directory params['path'] do
    owner 'root'
    group 'root'
    mode params['directory mask']
    recursive true
  end
end
