#Chef vault
if node['samba4-ad']['chef-vault']
  include_recipe 'chef-vault'
  secrets = chef_vault_item(node['samba4-ad']['data-bag'], node['samba4-ad']['data-bag-item'])
else
  secrets = data_bag_item(node['samba4-ad']['data-bag'], node['samba4-ad']['data-bag-item'])
end

#Fix avahi config if available
service "avahi-daemon" do
  action :nothing
end

execute "fix avahi config" do
  command "sudo sed -i 's/^#domain-name=.*$/domain-name=.alocal/' /etc/avahi/avahi-daemon.conf"
  notifies :restart, 'service[avahi-daemon]', :immediately
  only_if { ::File.exist?('/etc/avahi/avahi-daemon.conf')}
end

#Install required client packages
node['samba4-ad']['client_packages'].each do |pkg|
  package pkg
end

# Kerberos Config
template '/etc/krb5.conf' do
  not_if ("grep \"server role = active directory\" /etc/samba/smb.conf")
end

# Samba smb.conf
template '/etc/samba/smb.conf' do
  source 'smb.conf.erb'
  owner 'root'
  group 'root'
  mode 00644
  unless node['samba4-ad']['shares'].nil?
    variables :shares => node['samba4-ad']['shares']
  end
  ['smbd', 'nmbd'].each do |s|
    notifies :restart, "service[#{s}]"
  end
end

# SSSD Config
template '/etc/sssd/sssd.conf' do
  owner 'root'
  group 'root'
  mode '0600'
end

# Restart Services
%w(ntp smbd nmbd).each do |svc|
  service svc do
    action :restart
  end
end

# Start SSSD Service
service 'sssd' do
  action :start
end

# Join to Domain
if node['samba4-ad']['client_join_domain']
  template '/usr/share/pam-configs/my_mkhomedir' do
    source 'my_mkhomedir.erb'
    owner 'root'
    group 'root'
    mode '0644'
    notifies :run, 'execute[pam-auth-update]'
  end

  execute 'pam-auth-update' do
    command 'pam-auth-update --package'
    action :nothing
  end

  execute "join #{node['samba4-ad']['realm']} domain" do
    command "net ads join -U #{secrets['adminuser']}@"\
            "#{node['samba4-ad']['realm']}%"\
            "'#{secrets['adminpass']}'"
    live_stream true
    #sensitive true
    not_if "getent group 'Domain Admins'"
  end
end

lightDMDir='/etc/lightdm'

#LightDM Config
service 'lightdm' do
  action :nothing
end

cookbook_file "#{lightDMDir}/lightdm.conf" do
  notifies :restart, 'service[lightdm]', :delayed
  only_if { ::File.directory?(lightDMDir)}
end
