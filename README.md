# samba4-ad-cookbook

Cookbook for configuring Linux DCs. This cookbook builds them and joins them to an existing AD infrastructure or builds DCs in a new stand-alone AD forest.

In addition, this cookbook can be used to join linux member servers (clients) to an existing domain.

## Supported Platforms

Ubuntu 14.04
(Next step will be to support CentOS 7 and Ubuntu 16.04)


## Test

 - Run `kitchen create`
 - Edit .kitchen.yml and fix the IP addresses (The primary DC needs to point to itself and the secondary IP's and the secondary DC needs to point to itself and the primary's. The client needs to point to both)
 - Run `kitchen converge primary`
 - Log in and restart the primary server
 - Run `kitchen converge secondary`
 - Log in and restart the secondary server
 - Run `kitcehn converge client`

# Verify AD Server

Check Shares:
```
vagrant@dc1:~$ smbclient -L localhost -U%
Domain=[TEST] OS=[Windows 6.1] Server=[Samba 4.3.11-Ubuntu]

	Sharename       Type      Comment
	---------       ----      -------
	netlogon        Disk
	sysvol          Disk
	IPC$            IPC       IPC Service (Samba 4.3.11-Ubuntu)
Domain=[TEST] OS=[Windows 6.1] Server=[Samba 4.3.11-Ubuntu]

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP
```

Use the smbclient command to do an ls on the netlogon share:
```
vagrant@dc1:~$ smbclient //localhost/netlogon -UAdministrator -c 'ls'
Enter Administrator's password:
Domain=[TEST] OS=[Windows 6.1] Server=[Samba 4.3.11-Ubuntu]
  .                                   D        0  Wed Nov 16 08:58:16 2016
  ..                                  D        0  Wed Nov 16 08:58:20 2016

		40387156 blocks of size 1024. 36316688 blocks available
```

Verify DNS Entries (both DCs should show up here):
```
vagrant@dc1:~$ host -t SRV _kerberos._udp.test.local.
_kerberos._udp.test.local has SRV record 0 100 88 dc1.test.local.

vagrant@dc1:~$ host -t SRV _ldap._tcp.test.local.
_ldap._tcp.test.local has SRV record 0 100 389 dc1.test.local.

vagrant@dc1:~$ host -t A dc1.test.local.
dc1.test.local has address 10.211.55.211
```

The secondary server should connect to the primary and join it.

Log onto both of the DC's and run this command to show replication of AD partitions
```
sudo samba-tool drs showrepl
```

If one fails, you may have to see if there are DNS entries for both DC's and maybe restart some things


## License and Authors

Author: John Reed (<serverninja@gmail.com>)

Copyright [2016] [John W. Reed II]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

```
    http://www.apache.org/licenses/LICENSE-2.0
```

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
